# Git Workshop Exercise 2

## Person B Instructions

1. Clone your partner's repository into your local machine after he/she finished setting up the project. The link should be: (https://gitlab.com/<PARTNER'S_USERNAME>/git-workshop).
2. Go to your local repository and create a new branch `exercise2-solution` and switch to that branch.
3. Write the code for the recursive solution of factorial in `algorithms.py` using the pseudocode given below.

```
Function RecursiveFactorial(int n)
    if n >= 1 do
        return n * RecursiveFactorial(n-1)
    else
        return 1
    end
end
```
4. Commit your changes.
5. Go to the `master` branch and pull your partner's changes. Wait for him/her to finish his/her part (Step 8 of his/her instruction sheet) before you can do this.
6. Go back to `exercise2-solution` branch and rebase that branch with `master` branch.
7. In `program.py`, add code in the main function that allow the program to prompt the user to enter a number and print the factorial of that number using the `RecursiveFactorial` function that you have written.
8. Commit your changes.
9. Merge your `exercise2-solution` branch to the `master` branch.
10. Push your `master` branch to update your online repository.
11. Congratulations! You are done! :) If both you and your partner were able to finish, redo the exercise but this time, you would exchange roles by following the instructions from the other instruction sheet.
