# Git Workshop Exercise 2

## Person A Instructions

1. Fork [this repository](https://gitlab.com/billonesciprian08/git-workshop) from gitlab.
2. Add your partner as contributor of your forked project by clicking `Settings` then `Members` from the sidebar of the repository.
3. Clone **YOUR FORKED REPOSITORY** in your local machine.
4. Go to your local repository and create a new branch `exercise2-solution` and switch to that branch.
5. Write the code for the iterative solution of factorial in `algorithms.py` using the pseudocode given below.

```
Function IterativeFactorial(int n)
    product = 1
    counter = 1
    while counter <= n do
        product = product * counter
    end
    return product
end
```
6. Commit your changes.
7. Merge your `exercise2-solution` branch to the `master` branch.
8. Push your `master` branch to update your online repository.
9. Go back to `exercise2-solution` branch.
10. In `program.py`, add code in the main function that allow the program to prompt the user to enter a number and print the factorial of that number using the `IterativeFactorial` function that you have written.
11. Commit your changes.
12. Go back to the `master` branch and pull your partner's changes. Wait for him/her to finish his/her part (Step 10 of his/her instruction sheet) before you can do this.
13. Go back to `exercise2-solution` branch and rebase that branch with `master` branch.
14. Merge `exercise2-solution` branch to the `master` branch and push your changes.
15. Congratulations! You are done! :) If both you and your partner were able to finish, redo the exercise but this time, you would exchange roles by following the instructions from the other instruction sheet.
