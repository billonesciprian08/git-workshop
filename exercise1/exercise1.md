# Git Workshop Exercise 1

This hands-on exercise is an individual activity to help each learner familiarize themselves with the basic git commands without the need to collaborate with other programmers yet.

Learners are expected to know basic python to write a simple application and a solution to the sample problem.

## Instructions

1. Create a private repository name `'git-basics-exercise'` in https://about.gitlab.com/.
2. Clone the repository you created in your local machine.
3. Create a simple `"Git Gud"` program with a filename '"exercise1.py"` for your initial commit and push your changes to the remote repository.

```
# Git Gud Program Pseudocode
# File: exercise.py

Function Main:
	output “Git Gud”
```

4. Create a python script named `"solution1.py"` and write a function for the first solution of the `"Finding the sum of all multiples of 3 or 5 below 1000"` problem given the pseudocode provided:

```
# Solution 1 Pseudocode
# File: solution1.py				

Function SumDivisibleBy(target, n)		
	p = target / n						
	return n * (p * (p + 1)) / 2				
EndFunction			
```

5. Commit and push your changes.
6. In your `"Git Gud"` program, import the `"solution1.py"` script that you have written and call the method below inside the main method:

```
print(SumDivisibleBy(1000, 3) + SumDivisibleBy(1000, 5) - SumDivisibleBy(1000, 15)) 
```

7. Commit and push your changes.
8. Create another python script named `"solution2.py"` and write the other solution for the same problem from (4) using the pseudocode below:

```
Function LoopSolution(target):
	sum = 0
	for i = 1 to target do
		if (i mod 3 = 0) or (i mod 5 = 0) then sum := sum + i
	Return sum
```

9. Commit and push your changes.
10. You realized that the first solution for the given problem is much more efficient than the second solution, hence, your last commit is really not needed. Undo the latest commit (from steps 6-7) then push your final changes in the remote repository.